public class POOArticle{
    public int ID;
    public String title;
    public String author;
    public String content;
    public int evaluationCount;
    public static final int MAXEVAL = 100;
    public String [] evaluationMessages;
    private int countLine;
    private static int countID = 0;

    public POOArticle(String inputTitle, String inputAuthor, String inputContent){
        evaluationMessages = new String[MAXEVAL];
        evaluationCount = 0;
        countLine = 0;
        title = inputTitle;
        author = inputAuthor;
        content = inputContent;
        ID = countID++;
    }
    public void push(String input){
        if(countLine == MAXEVAL){
            System.out.println("message is full.");
            return;
        }
        evaluationCount++;
        evaluationMessages[countLine++] = "push:" + input;
    }
    public void boo(String input){
        if(countLine == MAXEVAL){
            System.out.println("message is full.");
            return;
        }
        evaluationCount--;
        evaluationMessages[countLine++] = "boo:" + input;
    }
    public void arrow(String input){
        if(countLine == MAXEVAL){
            System.out.println("message is full.");
            return;
        }
        evaluationMessages[countLine++] = "-> " + input;
    }
    public void show(){
        System.out.println("-evaluation count: " + evaluationCount);
        System.out.println("|ID: " + ID);
        System.out.println("|title: " + title);
        System.out.println("-author: " + author);
        System.out.println("---content---");
        System.out.println(content);

        if(countLine == 0){
            System.out.println();
            return;
        }

        System.out.println("---response---");
        for(int i = 0;i < countLine;i++)
            System.out.println(evaluationMessages[i]);
        System.out.println();
    }
    public void list(){
        System.out.println("-evaluation count: " + evaluationCount);
        System.out.println("|ID: " + ID);
        System.out.println("|title: " + title);
        System.out.println("-author: " + author);
        System.out.println();
    }
}
