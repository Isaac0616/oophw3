public class POOBoard{
    public int tail;
    public int countArticle;
    public String name;
    public POOArticle [] articles;
    private static final int MAXSIZE = 1024;

    public POOBoard(String inputName){
        tail = 0;
        countArticle = 0;
        name = inputName;
        articles = new POOArticle[MAXSIZE];
    }
    public void add(POOArticle article){
        if(tail == MAXSIZE){
            System.out.println("board is full.");
            return;
        }
        articles[tail++] = article;
        countArticle++;
    }
    public void del(int pos){
        articles[pos] = null;
        countArticle--;
    }
    public void move(int src, int dest){
        if(articles[src] == null || articles[dest] != null || dest >= tail){
            System.out.println("illegal move.");
            return;
        }
        articles[dest] = articles[src];
        articles[src] = null;
    }
    public int length(){
        return countArticle;
    }
    public void show(){
        System.out.println("Board: " + name);
        for(int i = 0;i < tail;i++){
            if(articles[i] != null)
                System.out.println(articles[i].title);
            else
                System.out.println("deleted");
        }
        System.out.println();
    }
}
