public class POODirectory{
    private static final int MAXSIZE = 1024;
    private static final byte EMPTY = 0;
    private static final byte BOARD = 1;
    private static final byte DIR = 2;
    private static final byte SPLIT = 3;
    byte [] type;
    POOBoard [] boards;
    POODirectory [] directorys;
    public String name;
    public int tail;
    public int count;

    public POODirectory(String inputName){
        tail = 0;
        count = 0;
        name = inputName;
        type = new byte[MAXSIZE];
        boards = new POOBoard[MAXSIZE];
        directorys = new POODirectory[MAXSIZE];
    }
    public void add(POOBoard board){
        if(tail == MAXSIZE){
            System.out.println("directory is full.");
            return;
        }
        type[tail] = BOARD;
        boards[tail] = board;
        tail++;
        count++;
    }
    public void add(POODirectory dir){
        if(tail == MAXSIZE){
            System.out.println("directory is full.");
            return;
        }
        type[tail] = DIR;
        directorys[tail] = dir;
        tail++;
        count++;
    }
    public void addSplit(){
        if(tail == MAXSIZE){
            System.out.println("directory is full.");
            return;
        }
        type[tail++] = SPLIT;
        count++;
    }
    public void del(int pos){
        type[pos] = EMPTY;
        count--;
    }
    public void move(int src, int dest){
        if(type[src] == EMPTY || type[dest] != EMPTY || dest >= tail){
            System.out.println("illegal move.");
            return;
        }
        if(type[src] == BOARD){
            type[dest] = type[src];
            boards[dest] = boards[src];
            type[src] = EMPTY;
            boards[src] = null;
        }
        else if(type[src] == DIR){
            type[dest] = type[src];
            directorys[dest] = directorys[src];
            type[src] = EMPTY;
            directorys[src] = null;
        }
        else if(type[src] == SPLIT){
            type[dest] = type[src];
            type[src] = EMPTY;
        }
    }
    public int length(){
        return count;
    }
    public void show(){
        System.out.println("Directory: " + name);
        for(int i = 0;i < tail;i++){
            if(type[i] == EMPTY)
                System.out.println("deleted");
            else if(type[i] == BOARD)
                System.out.println(boards[i].name);
            else if(type[i] == DIR)
                System.out.println(directorys[i].name + "/");
            else
                System.out.println("--------------------");
        }
        System.out.println();
    }
}
