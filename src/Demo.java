public class Demo{
    public static void main(String [] argv){
        POOArticle article1 = new POOArticle("articleA", "authorA", "line1\nline2\nline3");
        article1.push("push1");
        article1.push("push2");
        article1.boo("boo1");
        article1.arrow("arrow1");
        article1.show();
        article1.list(); //create articleA

        POOArticle article2 = new POOArticle("articleB", "authorB", "line1\nline2\nline3\nline4\nline5");
        article2.push("push1");
        article2.push("push2");
        article2.boo("boo1");
        article2.arrow("arrow1");
        article2.boo("boo2");
        article2.boo("boo3");
        article2.arrow("arrow2");
        article2.show();
        article2.list(); //create articleB

        POOArticle article3 = new POOArticle("articleC", "authorC", "line1"); //create articleC

        POOBoard board1 = new POOBoard("boardA");
        board1.add(article1);
        System.out.println("length = " + board1.length());
        board1.show();
        board1.add(article2);
        System.out.println("length = " + board1.length());
        board1.show();
        board1.add(article3);
        System.out.println("length = " + board1.length());
        board1.show();
        board1.del(1);
        System.out.println("length = " + board1.length());
        board1.show();
        board1.move(0, 1);
        System.out.println("length = " + board1.length());
        board1.show(); // creat boardA

        POOBoard board2 = new POOBoard("boardB");
        board2.add(article2);
        board2.add(article3);
        board2.add(article1);
        board2.del(0);
        board2.add(article2);
        board2.del(2);
        board2.add(article1);
        board2.move(1, 0);
        System.out.println("length = " + board2.length());
        board2.show(); // creat boardB

        POODirectory dir1 = new POODirectory("dirA"); //create level1 dirA
        POODirectory dir2 = new POODirectory("dirB"); //create level2 dirB
        POODirectory dir3 = new POODirectory("dirC"); //create level2 dirC
        dir1.add(dir2);
        dir1.add(dir3);
        dir1.addSplit();
        dir1.add(board1);
        dir1.add(board2);
        dir1.addSplit();
        System.out.println("length = " + dir1.length());
        dir1.show();
        dir1.addSplit();
        dir1.addSplit();
        dir1.del(6);
        dir1.del(7);
        dir1.move(0,6);
        dir1.move(3,7);
        dir1.move(1,3);
        System.out.println("length = " + dir1.length());
        dir1.show();
    }
}
